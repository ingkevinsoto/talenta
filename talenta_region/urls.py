from django.conf.urls import url
from talenta_region import views

app_name = 'talenta_region'

urlpatterns = [
    #url(r'^list_student/', views.list_student, name='list_student'),
    url(r'^add_municipio/', views.MunicipioCreate.as_view(), name='add_municipio'),
    url(r'^add_region/', views.RegionCreate.as_view(), name='add_region'),
    url(r'^edit_municipio/(?P<pk>\d+)/$', views.MunicipioEdit.as_view(), name='edit_municipio'),
    url(r'^edit_region/(?P<pk>\d+)/$', views.RegionEdit.as_view(), name='edit_region'),
    url(r'^delete_municipio/(?P<pk>\d+)/$', views.MunicipioDelete.as_view(), name='delete_municipio'),
    url(r'^delete_region/(?P<pk>\d+)/$', views.RegionDelete.as_view(), name='delete_region'),
    url(r'^list_municipios/', views.ListMunicipio.as_view(), name='list_municipio'),
    url(r'^list_regiones/', views.ListRegion.as_view(), name='list_regiones'),
    #url(r'^subject-data/$', subject_data_list, name='subject-data-list'),
    #url(r'^subject-data/(?P<pk>[0-9]+)/$', subject_data_detail, name='subject-data-detail')

]