from django.db import models

# Create your models here.


class Municipio(models.Model):
    code = models.IntegerField(unique=True)
    name = models.CharField(max_length=150)
    STATUS = (
        ('Activo', 'Activo'),
        ('Inactivo', 'Inactivo')
    )
    status = models.CharField(max_length=20, choices=STATUS)

    def __str__(self):
        return u'{} {}'.format(self.name, self.code)


class Region(models.Model):
    code = models.IntegerField()
    name = models.CharField(max_length=150)
    municipios = models.ManyToManyField(Municipio)

    def __str__(self):
        return u'{} {}'.format(self.name, self.code)
