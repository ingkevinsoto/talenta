from django.views.generic import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.urls import reverse_lazy




# Create your views here.
from talenta_region.forms import MunicipioForm, RegionForm
from talenta_region.models import Municipio, Region


class ListMunicipio(ListView):
    template_name = 'list_municipio.html'
    model = Municipio


class ListRegion(ListView):
    template_name = 'list_region.html'
    model = Region


class MunicipioCreate(CreateView):
    model = Municipio
    form_class = MunicipioForm
    template_name = 'add_municipio.html'

    def get_success_url(self):
        return reverse_lazy('talenta_region:list_municipios')


class RegionCreate(CreateView):
    model = Region
    form_class = RegionForm
    template_name = 'add_region.html'

    def get_success_url(self):
        return reverse_lazy('talenta_region:list_regiones')


class MunicipioEdit(UpdateView):
    model = Municipio
    form_class = MunicipioForm
    template_name = 'edit_municipio.html'

    def get_success_url(self):
        return reverse_lazy('talenta_region:list_municipios')


class RegionEdit(UpdateView):
    model = Region
    form_class = RegionForm
    template_name = 'edit_region.html'

    def get_success_url(self):
        return reverse_lazy('talenta_region:list_regiones')


class MunicipioDelete(DeleteView):
    model = Municipio
    form_class = MunicipioForm
    template_name = 'delete_municipio.html'

    def get_context_data(self, **kwargs):
        context_data = super(MunicipioDelete, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        municipio = Municipio.objects.get(id=int(pk))
        context_data.update({'student': municipio})
        return context_data

    def get_success_url(self):
        return reverse_lazy('talenta_region:list_municipios')


class RegionDelete(DeleteView):
    model = Region
    form_class = RegionForm
    template_name = 'delete_region.html'

    def get_context_data(self, **kwargs):
        context_data = super(RegionDelete, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        region = Region.objects.get(id=int(pk))
        context_data.update({'student': region})
        return context_data

    def get_success_url(self):
        return reverse_lazy('talenta_region:list_regiones')


