from django.contrib import admin

# Register your models here.
from talenta_region.models import Municipio, Region


class MunicipioAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'status')
    list_filter = ('name',)

class RegionAdmin(admin.ModelAdmin):
    list_display = ('code', 'name',)
    list_filter = ('name',)


admin.site.register(Municipio, MunicipioAdmin)
admin.site.register(Region, RegionAdmin)
