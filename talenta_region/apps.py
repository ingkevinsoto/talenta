from django.apps import AppConfig


class TalentaRegionConfig(AppConfig):
    name = 'talenta_region'
