from django import forms
from talenta_region.models import Municipio, Region


class MunicipioForm(forms.ModelForm):
    class Meta:
        model = Municipio
        fields = '__all__'



class RegionForm(forms.ModelForm):
    class Meta:
        model = Region
        fields = '__all__'

        widgets = {
            'code': forms.TextInput(attrs={'class': 'form-control'}),
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'municipios': forms.SelectMultiple(attrs={'class': 'form-control'}),
        }

